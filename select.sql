-- IN, ANY, ALL
select distinct maker from product
where type = 'pc' and model not in (select maker from product where model in (select model from laptop));

select distinct maker from product
where (type = 'pc') and model <> all (select maker from product where model <> all (select model from laptop));

select distinct maker from product
where (type = 'pc') and model <> any (select maker from product where model <> any (select model from laptop));

select distinct maker from product
 where type in ('PC', 'Laptop') ;
 
 select distinct maker from product
 where type = any (select type from product where type in ('Laptop', 'PC')) ;

select distinct maker from product
 where type = all (select type from product where type ='Laptop')
 or type = all (select type from product where type ='PC') ;

select distinct maker from product 
where model = any (select model from pc);

select count(*) from product 
where type = 'PC' and maker = 'A';

select model, price from laptop 
where price > all (select price from pc);

select maker from product 
where exists(select maker from pc where pc.model = model);

select type, product.model, speed from laptop left join product
on laptop.model = product.model
 where speed < all (select model from pc where product.model = model);





