select concat("середня ціна") as name, avg(price) as value from laptop;

select concat("модель=", model) as model, concat("швидкість=", speed) as speed, concat("ціна=", price) from pc; 

select concat(YEAR(date),".",MONTH(date),".",DAY(date)) as date from income;

select ship, battle,
case result 
	when 'sunk' then 'потонув'
    when 'damaged' then 'поранений'
    when 'OK' then 'вцілів'
end as result
from outcomes;

select trip_no, date, ID_psg, 
concat("ряд= ", substring(place,1,1)) as plane_row, 
concat("місце= ", substring(place,2,1)) as place
 from pass_in_trip;
 
 select trip_no, plane,
 concat("from ", town_from, " to ", town_to) as from_to
 from trip;
