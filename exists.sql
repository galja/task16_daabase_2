-- EXISTS

select distinct pr.maker from product pr
where exists(select model from pc where pr.model = model and speed >=750);

select distinct pr.maker from product pr
where model in (select model from laptop where pr.model = model) and
exists(
	select maker from product prod 
		where model in(
			select model from pc where speed >=750)
			and pr.maker = prod.maker);
            
select distinct maker from product
where model in (select model from printer where product.model = model) 
and exists (
	select max(speed) from pc where (model = product.model)
);

select maker from product pr1
where exists(
	select maker from product
    where model = pr1.model
    and maker in (select maker from product pr2
					where pr2.model in (
						select model from printer
                    )
				)and maker in (select maker from product pr3 
								where pr3.model = pr1.model and pr3.model in(select model from pc
														where speed = (select max(speed) from pc)
                                                        ) )
);

select name, launched from ships
where exists (select class from classes 
				where displacement >= 35000 
                and class = ships.class);