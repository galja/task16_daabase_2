select model, price from printer
where price = (select max(price) from printer);

select type, laptop.model, speed from laptop left join product
on laptop.model = product.model
where speed < (select min(speed) from pc);

select maker, price, color from printer left join product
on printer.model = product.model
where (color like 'y') and price = (select min(price) from printer  where color like 'y');

select maker, count(model) as cou from product
where type like 'PC' 
group by maker
having cou >= 2;

select product.maker, avg(pc.hd) from pc, product
where product.model = pc.model
and product.maker in (select distinct maker from product 
						where product.type = 'printer')
group by maker;